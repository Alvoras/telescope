package blockchain

import (
	"fmt"
	"strings"
)

type Address struct {
	Hash160       string `json:"hash160"`
	Address       string `json:"address"`
	NTx           int    `json:"n_tx"`
	TotalReceived int    `json:"total_received"`
	TotalSent     int    `json:"total_sent"`
	FinalBalance  int    `json:"final_balance"`
	Txs           []*Tx  `json:"txs"`
}

type MultiAddr struct {
	Addresses []*Address `json:"addresses"`
	Txs       []*Tx      `json:"txs"`
}

type Tx struct {
	Result      int       `json:"result"`
	Ver         int       `json:"ver"`
	Size        int       `json:"size"`
	Inputs      []*Inputs `json:"inputs"`
	Time        int       `json:"time"`
	BlockHeight int       `json:"block_height"`
	TxIndex     int       `json:"tx_index"`
	VinSz       int       `json:"vin_sz"`
	Hash        string    `json:"hash"`
	VoutSz      int       `json:"vout_sz"`
	RelayedBy   string    `json:"relayed_by"`
	Out         []*Out    `json:"out"`
}

type Inputs struct {
	Sequence int      `json:"sequence"`
	Script   string   `json:"script"`
	PrevOut  *PrevOut `json:"prev_out"`
}

type PrevOut struct {
	Spent   bool   `json:"spent"`
	TxIndex int    `json:"tx_index"`
	Type    int    `json:"type"`
	Addr    string `json:"addr"`
	Value   int    `json:"value"`
	N       int    `json:"n"`
	Script  string `json:"script"`
}

type Out struct {
	Spent   bool   `json:"spent"`
	TxIndex int    `json:"tx_index"`
	Type    int    `json:"type"`
	Addr    string `json:"addr"`
	Value   int    `json:"value"`
	N       int    `json:"n"`
	Script  string `json:"script"`
}

func (c *Client) GetAddress(address string) (*Address, error) {
	rsp := &Address{}
	var path = "/address/" + address
	e := c.loadResponse(path, rsp, true)

	if e != nil {
		fmt.Print(e)
	}
	return rsp, e
}

func (c *Client) GetAddresses(addresses []string) (*MultiAddr, error) {
	rsp := &MultiAddr{}
	var path = "/multiaddr?active=" + strings.Join(addresses, "|")
	e := c.loadResponse(path, rsp, false)

	if e != nil {
		fmt.Print(e)
	}
	return rsp, e
}

func (addr Address) IncomingTxs() []Tx {
	var res []Tx

	for i := range addr.Txs {
		incoming, err := addr.Txs[i].IncomingTo(addr.Address)
		if err != nil {
			return res
		}

		if incoming {
			res = append(res, *addr.Txs[i])
		}
	}

	return res
}

func (addr Address) OutgoingTxs() []Tx {
	var res []Tx

	for i := range addr.Txs {
		outgoing, err := addr.Txs[i].OutgoingFrom(addr.Address)
		if err != nil {
			return res
		}
		if outgoing {
			res = append(res, *addr.Txs[i])
		}
	}

	return res
}

func (addr Address) IncomingTxsPtr() []*Tx {
	var res []*Tx

	for i := range addr.Txs {
		incoming, err := addr.Txs[i].IncomingTo(addr.Address)
		if err != nil {
			return res
		}

		if incoming {
			res = append(res, addr.Txs[i])
		}
	}

	return res
}

func (addr Address) OutgoingTxsPtr() []*Tx {
	var res []*Tx

	for i := range addr.Txs {
		outgoing, err := addr.Txs[i].OutgoingFrom(addr.Address)
		if err != nil {
			return res
		}
		if outgoing {
			res = append(res, addr.Txs[i])
		}
	}

	return res
}

func (tx *Tx) Addresses() []string {
	var addrPool []string

	for _, input := range tx.Inputs {
		addrPool = append(addrPool, input.PrevOut.Addr)
	}

	for _, out := range tx.Out {
		addrPool = append(addrPool, out.Addr)
	}

	return addrPool
}

func (tx *Tx) OutgoingFrom(addr string) (bool, error) {
	isAddressPresent := false
	for _, input := range tx.Inputs {
		if input.PrevOut.Addr == addr {
			isAddressPresent = true
		}
	}

	if !isAddressPresent {
		for _, out := range tx.Out {
			if out.Addr == addr {
				isAddressPresent = true
			}
		}
	}

	if !isAddressPresent {
		return false, fmt.Errorf("The given address is not present in this transaction")
	}

	for _, input := range tx.Inputs {
		if input.PrevOut.Addr == addr {
			return true, nil
		}
	}
	return false, nil
}

func (tx *Tx) IncomingTo(addr string) (bool, error) {
	res, err := tx.OutgoingFrom(addr)
	return !res, err
}

func (tx *Tx) GetRecipient() string {
	if tx.VoutSz == 1 && tx.VinSz == 1 {
		return tx.Inputs[0].PrevOut.Addr
	}

	for _, input := range tx.Inputs {
		// If an address is in the inputs and in the outputs at the same time,
		// then it must be a return address and thus the input address is the sender
		for _, out := range tx.Out {
			if input.PrevOut.Addr == out.Addr {
				return input.PrevOut.Addr
			}
		}
	}

	return ""
}

func (tx *Tx) GetSenders() []string {
	list := []string{}
	recipient := tx.GetRecipient()

	incoming, err := tx.IncomingTo(recipient)

	if err != nil {
		return list
	}

	if incoming {
		for _, input := range tx.Inputs {
			list = append(list, input.PrevOut.Addr)
		}
	} else {
		for _, out := range tx.Out {
			list = append(list, out.Addr)
		}
	}

	return list
}
