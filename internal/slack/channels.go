package slack

import (
	"fmt"
	"strings"

	"gitlab.com/Alvoras/telescope/internal/context"
)

func CreateChannel(name string) (string, error) {
	channel, err := context.SlackAppClient.JoinChannel(name)
	if err != nil {
		return "", err
	}

	_, err = context.SlackAppClient.JoinChannel(name)

	return channel.ID, err
}

func GetChannelID(name string) (string, error) {
	channels, err := context.SlackAppClient.GetChannels(false)
	if err != nil {
		return "", err
	}

	for _, channel := range channels {
		// Slack limit the channels name to 21 characters.......
		if strings.EqualFold(channel.Name, name[:21]) {
			return channel.ID, nil
		}
	}

	return "", fmt.Errorf("The channel %s does not exist", name)
}

func ChannelExists(name string) (bool, error) {
	channels, err := context.SlackAppClient.GetChannels(false)
	if err != nil {
		return false, err
	}

	for _, channel := range channels {
		// Slack limit the channels name to 21 characters.......
		if strings.EqualFold(channel.Name, name[:21]) {
			return true, nil
		}
	}

	return false, nil
}

func CreateChannelIfNotExist(name string) (string, error) {
	exists, err := ChannelExists(name)
	if err != nil {
		return "", err
	}

	if exists {
		return "", fmt.Errorf("The channel %s already exists", name)
	}

	return CreateChannel(name)
}

func IsChannelArchived(name string) (bool, error) {
	channels, err := context.SlackAppClient.GetChannels(false)
	if err != nil {
		return false, err
	}

	for _, channel := range channels {
		// Slack limit the channels name to 21 characters.......
		if strings.EqualFold(channel.Name, name[:21]) {
			return channel.IsArchived, nil
		}
	}

	return false, nil
}
