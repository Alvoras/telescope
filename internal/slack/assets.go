package slack

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/Alvoras/telescope/internal/config"
	"gitlab.com/Alvoras/telescope/internal/context"
	"gitlab.com/Alvoras/telescope/internal/defs"
	"gitlab.com/Alvoras/telescope/internal/logger"
	"gitlab.com/Alvoras/telescope/internal/utils"
	"gitlab.com/Alvoras/telescope/internal/wsbchain"
)

func RegisterNewAsset(cid string, args []string) {
	addr, comment := args[0], args[1:]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	if ok {
		SendMessage(cid, fmt.Sprintf("L'adresse %s est déjà enregistrée", addr), nil)
		// Log error
		return
	}

	now := time.Now().Format("2006-01-02 15:04:05")

	asset := defs.Asset{
		Address:  addr,
		Created:  now,
		Updated:  now,
		Comment:  strings.Join(comment, " "),
		IsActive: true,
	}

	if err := context.Db.Write("assets", addr, asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	// Create asset channel
	channelID, err := CreateChannelIfNotExist(addr)
	if err != nil && !strings.Contains(err.Error(), "already exists") {
		SendMessage(config.Cfg.Channels["errors"], fmt.Sprintf("Failed to create channel : %s", err.Error()), nil)
		logger.Error.Println(err)
		return
	}

	err = EditChannelID(addr, channelID)
	if err != nil {
		SendMessage(cid, fmt.Sprintf("[✖] Failed to write the asset's channel ID %s : %s", addr, err), nil)
	}

	logger.Info.Println(fmt.Sprintf("Subscribing to the %s feed", addr))
	wsbchain.SubscribeToAddr(addr)

	SendMessage(cid, fmt.Sprintf("[✔] L'adresse %s a bien été ajouté au suivi", addr), nil)
}

func DeactivateAsset(cid string, args []string) {
	addr := args[0]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	asset := defs.Asset{}

	if err = context.Db.Read("assets", addr, &asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	_, err = ResyncChan(addr)
	if err != nil {
		SendMessage(cid, fmt.Sprintf("[✖] Failed to sync the asset %s : %s", addr, err), nil)
		return
	}

	isArchived, err := IsChannelArchived(addr)
	if err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
	}

	if !isArchived {
		ArchiveChannel(cid, []string{addr})
	}

	if !asset.IsActive {
		SendMessage(cid, fmt.Sprintf("L'adresse %s est déjà désactivée", addr), nil)
		return
	}

	asset.IsActive = false
	asset.Updated = time.Now().Format("2006-01-02 15:04:05")

	if err := context.Db.Write("assets", addr, asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	logger.Info.Println(fmt.Sprintf("Unsubscribing from the %s feed", addr))
	wsbchain.UnsubscribeToAddr(addr)

	SendMessage(cid, fmt.Sprintf("[✔] L'adresse %s ne sera plus suivie", addr), nil)
}

func ActivateAsset(cid string, args []string) {
	var channelID string
	addr := args[0]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	asset := defs.Asset{}

	if err = context.Db.Read("assets", addr, &asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	isArchived, err := IsChannelArchived(addr)

	if err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
	}

	if isArchived {
		RestoreChannel(cid, []string{addr})
	} else {
		channelID, _ = CreateChannelIfNotExist(addr)
		if asset.ChannelID == "" || channelID == "" {
			_, err := ResyncChan(addr)
			if err != nil {
				SendMessage(cid, fmt.Sprintf("[✖] Failed to sync the asset %s : %s", addr, err), nil)
				return
			}
		}

	}

	if asset.IsActive {
		SendMessage(cid, fmt.Sprintf("L'adresse %s est déjà activée", addr), nil)
		return
	}

	logger.Info.Println(fmt.Sprintf("Subscribing to the %s feed", asset.Address))
	wsbchain.SubscribeToAddr(addr)

	SendMessage(cid, fmt.Sprintf("[✔] L'adresse %s est maintenant suivie", addr), nil)
}

func CheckChan(cid string, args []string) {
	addr := args[0]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	if _, err := ResyncChan(addr); err != nil {
		SendMessage(cid, fmt.Sprintf("[✖] Failed to sync the asset %s : %s", addr, err), nil)
		return
	}

	SendMessage(cid, fmt.Sprintf("[✔] Le channel de l'adresse %s a été resynchronisé", addr), nil)
}

func ShowComment(cid string, args []string) {
	addr := args[0]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	asset := defs.Asset{}

	if err := context.Db.Read("assets", addr, &asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	SendMessage(cid, fmt.Sprintf("Commentaire associé à %s :\n`%s`", addr, asset.Comment), nil)
}

func ShowAsset(cid string, args []string) {
	addr := args[0]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	asset := defs.Asset{}

	if err := context.Db.Read("assets", addr, &asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	SendMessage(cid, "", utils.FmtAssetAttachment(asset))
}

func ShowAllAssets(cid string, args []string) {
	var err error
	var assets []defs.Asset

	if assets, err = utils.FetchAllAssets(); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	for _, asset := range assets {
		SendMessage(cid, "", utils.FmtAssetAttachment(asset))
	}
}

func EditAsset(cid string, args []string) {
	addr := args[0]
	args = args[1:]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	asset := defs.Asset{}

	if err := context.Db.Read("assets", addr, &asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	cmd := args[0]
	args = args[1:]

	value := strings.Join(args, " ")

	asset.Updated = time.Now().Format("2006-01-02 15:04:05")

	switch cmd {
	case "comment":
		asset.Comment = value
	case "alias":
		asset.Alias = value
	case "channelID":
		asset.ChannelID = value
	default:
		SendMessage(cid, "[✖] Unknown field.\nEditable settings : comment, alias", nil)
		return
	}

	if err := context.Db.Write("assets", addr, asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err)
		return
	}

	SendMessage(cid, fmt.Sprintf("[✔] L'asset %s a bien été mise à jour", addr), nil)
}

func ArchiveChannel(cid string, args []string) {
	addr := args[0]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	// toArchiveCid, err := GetChannelID(addr)
	// if err != nil {
	// 	SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
	// 	return
	// }
	//
	// asset.ChannelID = ""
	// asset.Updated = time.Now().Format("2006-01-02 15:04:05")
	//
	// if err := context.Db.Write("assets", addr, asset); err != nil {
	// 	SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
	// 	return
	// }

	toArchiveCid, err := ResyncChan(addr)
	if err != nil {
		SendMessage(cid, fmt.Sprintf("[✖] Failed to sync the asset %s : %s", addr, err), nil)
		return
	}

	if err := context.SlackAppClient.ArchiveChannel(toArchiveCid); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		return
	}

	SendMessage(cid, fmt.Sprintf("[✔] Le channel %s a bien été archivé", addr), nil)
}

func RestoreChannel(cid string, args []string) {
	addr := args[0]

	ok, err := utils.AssetExists(addr)
	if err != nil {
		return
	}

	if !ok {
		SendMessage(cid, fmt.Sprintf("[✖] L'adresse %s n'est pas enregistrée", addr), nil)
		return
	}

	toRestoreCid, err := ResyncChan(addr)
	if err != nil {
		SendMessage(cid, fmt.Sprintf("[✖] Failed to sync the asset %s : %s", addr, err), nil)
		return
	}

	if err := context.SlackAppClient.UnarchiveChannel(toRestoreCid); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		return
	}

	SendMessage(cid, fmt.Sprintf("[✔] Le channel %s a bien été restoré", addr), nil)
}

func EditChannelID(addr string, channelID string) error {
	asset, err := utils.LoadAsset(addr)
	if err != nil {
		return err
	}

	asset.ChannelID = channelID
	asset.Updated = time.Now().Format("2006-01-02 15:04:05")

	if err := context.Db.Write("assets", addr, asset); err != nil {
		return err
	}

	return nil
}

func ResyncChan(addr string) (string, error) {
	asset := defs.Asset{}

	if err := context.Db.Read("assets", addr, &asset); err != nil {
		return "", err
	}

	channelID, err := GetChannelID(addr)
	if err != nil {
		return "", err
	}

	asset.ChannelID = channelID
	asset.Updated = time.Now().Format("2006-01-02 15:04:05")

	if err := context.Db.Write("assets", addr, asset); err != nil {
		return "", err
	}

	return channelID, nil
}
