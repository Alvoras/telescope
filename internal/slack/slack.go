package slack

import (
	"strings"

	slacklib "github.com/nlopes/slack"

	"gitlab.com/Alvoras/telescope/internal/config"
	"gitlab.com/Alvoras/telescope/internal/context"
	"gitlab.com/Alvoras/telescope/internal/logger"
	"gitlab.com/Alvoras/telescope/internal/utils"
)

func Start() {
	rtm := context.SlackBotClient.NewRTM()
	go rtm.ManageConnection()

	go func() {
		for msg := range rtm.IncomingEvents {
			switch ev := msg.Data.(type) {
			case *slacklib.MessageEvent:
				handleMessage(ev)
			case *slacklib.ConnectedEvent:
				context.Bot = ev.Info.User
			default:
			}
		}
	}()

	<-context.EndSlackSessionChan
	logger.Info.Println("Closing slack session...")
	rtm.Disconnect()

	context.SlackSessionClosed <- true
	logger.Success.Println("Slack session terminated")
}

func handleMessage(ev *slacklib.MessageEvent) {
	if ev.User == context.Bot.ID || !utils.AuthorIsAuthorized(ev.User, config.Cfg.AuthorizedUsers) {
		return
	}

	cmds := strings.Split(ev.Text, "\n")
	for _, cmd := range cmds {
		// Bot intent must start with '.'
		if !strings.HasPrefix(cmd, ".") {
			return
		}
		line := strings.Split(cmd, " ")

		command, args := line[0], line[1:]

		command = command[1:]

		_ = args
		switch command {
		case "ping":
			SendMessage(ev.Channel, "pong", nil)
		case "register":
			RegisterNewAsset(ev.Channel, args)
		case "remove":
			DeactivateAsset(ev.Channel, args)
		case "activate":
			ActivateAsset(ev.Channel, args)
		case "comment":
			ShowComment(ev.Channel, args)
		case "show":
			ShowAsset(ev.Channel, args)
		case "assets":
			ShowAllAssets(ev.Channel, args)
		case "edit":
			EditAsset(ev.Channel, args)
		case "delete_chan":
			ArchiveChannel(ev.Channel, args)
		case "restore_chan":
			RestoreChannel(ev.Channel, args)
		case "chkchan":
			CheckChan(ev.Channel, args)
		}
	}
}
