package slack

import (
	"fmt"
	"strconv"
	"time"

	slacklib "github.com/nlopes/slack"
	"gitlab.com/Alvoras/telescope/internal/config"
	"gitlab.com/Alvoras/telescope/internal/context"
	"gitlab.com/Alvoras/telescope/internal/defs"
	"gitlab.com/Alvoras/telescope/internal/logger"
	"gitlab.com/Alvoras/telescope/internal/utils"
)

func AlertNewTx(txMsg defs.TxMsg) {
	var value int
	var txType string
	var resSymbol string
	var arrow string

	tx := txMsg.SubTx.Tx
	addr := txMsg.Address

	asset := defs.Asset{}

	if err := context.Db.Read("assets", addr, &asset); err != nil {
		SendMessage(config.Cfg.Channels["errors"], err.Error(), nil)
		logger.Error.Println(err.Error())
		return
	}

	incoming, err := tx.IncomingTo(addr)
	if err != nil {
		logger.Error.Println(err)
		return
	}

	if incoming {
		for _, out := range tx.Out {
			if out.Addr == addr {
				value = out.Value
			}
		}

		txType = "incoming"
	} else {
		for _, input := range tx.Inputs {
			value += input.PrevOut.Value
		}

		for _, out := range tx.Out {
			if out.Addr == addr {
				value -= out.Value
			}
		}

		txType = "outgoing"
	}

	BTCValue := float64(value) / 100000000

	price24Hr, USDValue, err := utils.SatToDollars(BTCValue)
	if err != nil {
		return
	}

	switch txType {
	case "incoming":
		resSymbol = "+"
		arrow = ":arrow_left_green:"
	case "outgoing":
		resSymbol = "-"
		arrow = ":arrow_right_red:"
	}

	name := asset.Address
	if asset.Alias != "" {
		name = asset.Alias
	}

	comment := "N/A"
	if asset.Comment != "" {
		comment = asset.Comment
	}

	strUSDValue := fmt.Sprintf("%.2f", USDValue)

	attachment := &slacklib.Attachment{
		Pretext: "",
		Text:    fmt.Sprintf("New transaction associated with %s", name),
		// Uncomment the following part to send a field too
		Fields: []slacklib.AttachmentField{
			slacklib.AttachmentField{
				Title: "Address",
				Value: addr,
			},
			slacklib.AttachmentField{
				Title: "Timestamp",
				Value: time.Now().Format("2006-01-02 15:04:05"),
			},
			slacklib.AttachmentField{
				Title: "Result (BTC)",
				Value: fmt.Sprintf("%s%f฿ %s", resSymbol, BTCValue, arrow),
			},
			slacklib.AttachmentField{
				Title: "Result (USD)",
				Value: fmt.Sprintf("%s$%s %s", resSymbol, utils.FmtNumberString(strUSDValue), arrow),
			},
			slacklib.AttachmentField{
				Title: "Current BTC/USD (weighted 24h)",
				Value: fmt.Sprintf("$%s", utils.FmtNumberString(price24Hr)),
			},
			slacklib.AttachmentField{
				Title: "History",
				Value: fmt.Sprintf("%d transactions recorded", len(asset.History)),
			},
			slacklib.AttachmentField{
				Title: "Comment",
				Value: comment,
			},
		},
	}

	strBTCValue := strconv.FormatFloat(BTCValue, 'f', -1, 64)

	historyTx := defs.HistoryTx{
		Tx:        txMsg.SubTx.Tx,
		BTCValue:  strBTCValue,
		USDValue:  strUSDValue,
		Price24Hr: price24Hr,
	}

	go func() {
		if err := utils.AddToHistory(addr, historyTx); err != nil {
			logger.Error.Println("Failed to add the transaction to the asset history")
		}
	}()

	go func() {
		logger.Info.Println("Sending a message to signal a new transaction")
		SendMessage(asset.ChannelID, "", attachment)
	}()
}
