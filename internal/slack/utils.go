package slack

import (
	"strings"

	slacklib "github.com/nlopes/slack"
	"gitlab.com/Alvoras/telescope/internal/context"
)

func SendMessage(cid string, msg string, pAttachment *slacklib.Attachment) error {
	var attachment slacklib.Attachment

	if pAttachment != nil {
		attachment = *pAttachment
	}

	_, _, err := context.SlackBotClient.PostMessage(
		cid,
		slacklib.MsgOptionText(msg, false),
		slacklib.MsgOptionAttachments(attachment),
	)

	if err != nil {
		return err
	}

	return nil
}

func SyncAssetsChannels() error {
	channels, err := context.SlackAppClient.GetChannels(false)
	if err != nil {
		return err
	}

	for _, asset := range context.CachedAssets {
		for _, channel := range channels {
			// Slack limit the channels name to 21 characters.......
			if strings.EqualFold(channel.Name, asset.Address[:21]) {
				EditChannelID(asset.Address, channel.ID)
				break
			}
		}
	}

	return nil
}
