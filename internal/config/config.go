package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/Alvoras/telescope/internal/logger"
)

var (
	Cfg = new(Config)
)

// Config structure which mirrors the json file
type Config struct {
	Filepath        string
	URL             string            `json:"ws_api_url"`
	Price24HURL     string            `json:"price_24h_url"`
	Channels        map[string]string `json:"channels"`
	AuthorizedUsers map[string]string `json:"authorized_users"`
	AuthToken       string            `json:"auth_token"`
	AppToken        string            `json:"app_token"`
	BotToken        string            `json:"bot_token"`
	Timeout         struct {
		WSHandshake string `json:"wshandshake"`
	} `json:"timeout"`
}

func (cfg *Config) Load() {
	cfgData, err := ioutil.ReadFile(cfg.Filepath)
	if err != nil {
		logger.Error.Println("Failed to load config file", err)
	}

	if err := json.Unmarshal(cfgData, &cfg); err != nil {
		logger.Error.Println(fmt.Sprintf("Failed to load the config file (%s)", cfg.Filepath))
		logger.Error.Println(err)
		os.Exit(1)
	}

}
