package defs

import (
	"github.com/alfg/blockchain"
)

type Asset struct {
	Address   string      `json:"Address"`
	Alias     string      `json:"Alias"`
	Updated   string      `json:"Updated"`
	Created   string      `json:"Created"`
	Comment   string      `json:"Comment"`
	IsActive  bool        `json:"IsActive"`
	ChannelID string      `json:"ChannelID"`
	History   []HistoryTx `json:"History"`
}

type HistoryTx struct {
	Tx        blockchain.Tx
	BTCValue  string
	USDValue  string
	Price24Hr string
}

type SubTx struct {
	Op string        `json:"op"`
	Tx blockchain.Tx `json:"x"`
}

type TxMsg struct {
	Address string
	SubTx   *SubTx
}
