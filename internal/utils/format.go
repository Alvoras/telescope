package utils

import (
	"fmt"
	"strings"

	slacklib "github.com/nlopes/slack"
	"gitlab.com/Alvoras/telescope/internal/defs"
)

func FmtAssetAttachment(asset defs.Asset) *slacklib.Attachment {
	isActive := "No"
	if asset.IsActive {
		isActive = "Yes"
	}

	name := asset.Address
	if asset.Alias != "" {
		name = asset.Alias
	}

	comment := "None"
	if asset.Comment != "" {
		comment = asset.Comment
	}

	attachment := &slacklib.Attachment{
		Pretext: fmt.Sprintf("Asset : `%s`", name),
		Text:    "",
		// Uncomment the following part to send a field too
		Fields: []slacklib.AttachmentField{
			slacklib.AttachmentField{
				Title: "Address",
				Value: asset.Address,
			},
			slacklib.AttachmentField{
				Title: "Is active",
				Value: isActive,
			},
			slacklib.AttachmentField{
				Title: "Created",
				Value: asset.Created,
			},
			slacklib.AttachmentField{
				Title: "Last updated",
				Value: asset.Updated,
			},
			slacklib.AttachmentField{
				Title: "History",
				Value: fmt.Sprintf("%d transactions recorded", len(asset.History)),
			},
			slacklib.AttachmentField{
				Title: "Comment",
				Value: comment,
			},
		},
	}

	return attachment
}

func FmtNumberString(num string) string {
	var res []byte
	var cents string
	hasCents := false

	if strings.Contains(num, ".") {
		hasCents = true
		split := strings.Split(num, ".")
		num, cents = split[0], split[1]
	}

	cnt := len(num)

	num = ReverseString(num)

	for i := 0; i < len(num); i++ {
		if cnt%3 == 0 && i != 0 {
			res = append(res, ',')
		}

		res = append(res, num[cnt-1])
		cnt--
	}

	if hasCents {
		return strings.Join([]string{string(res), cents}, ".")
	}

	return string(res)
}
