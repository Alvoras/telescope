package utils

import (
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/Alvoras/telescope/internal/config"
)

func Get24HrPrice() (string, error) {
	url := config.Cfg.Price24HURL
	res, err := http.Get(url)
	if err != nil {
		return "", err
	}

	text, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(text), nil
}

func SatToDollars(btcPrice float64) (string, float64, error) {
	dolPrice, err := Get24HrPrice()
	if err != nil {
		return "", 0, err
	}

	fDolPrice, err := strconv.ParseFloat(dolPrice, 32)
	if err != nil {
		return "", 0, err
	}

	dolValue := float64(fDolPrice) * btcPrice

	return dolPrice, dolValue, nil
}
