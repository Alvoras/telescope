package utils

import "strings"

func StrInMapString(needle string, haystack map[string]string) bool {
	for _, val := range haystack {
		if needle == val {
			return true
		}
	}

	return false
}

func EscapeChar(str string, c string) string {
	return strings.Replace(str, c, `\`+c, -1)
}

// Logic from https://stackoverflow.com/questions/1752414/how-to-reverse-a-string-in-go
func ReverseString(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}
