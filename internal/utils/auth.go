package utils

func AuthorIsAuthorized(authorID string, authorized map[string]string) bool {
	return StrInMapString(authorID, authorized)
}
