package utils

import (
	"encoding/json"
	"time"

	"gitlab.com/Alvoras/telescope/internal/context"
	"gitlab.com/Alvoras/telescope/internal/defs"
)

func FetchAllAssets() ([]defs.Asset, error) {
	res := []defs.Asset{}
	records, err := context.Db.ReadAll("assets")
	if err != nil {
		return res, err
	}
	var asset defs.Asset

	for _, rec := range records {
		json.Unmarshal([]byte(rec), &asset)

		// Skip bad result
		if asset.Address == "" {
			continue
		}
		res = append(res, asset)
	}

	context.CachedAssets = res
	return res, nil
}

func AssetExists(addr string) (bool, error) {
	assets, err := FetchAllAssets()

	if err != nil {
		return false, err
	}

	for _, asset := range assets {
		if addr == asset.Address {
			return true, nil
		}
	}

	return false, nil
}

func AddToHistory(addr string, historyTx defs.HistoryTx) error {
	asset, err := LoadAsset(addr)
	if err != nil {
		return err
	}

	asset.History = append(asset.History, historyTx)
	asset.Updated = time.Now().Format("2006-01-02 15:04:05")

	if err := context.Db.Write("assets", addr, asset); err != nil {
		return err
	}

	return nil
}

func LoadAsset(addr string) (*defs.Asset, error) {
	asset := &defs.Asset{}

	if err := context.Db.Read("assets", addr, &asset); err != nil {
		return nil, err
	}

	return asset, nil
}

func LoadAssetHistory(addr string) ([]defs.HistoryTx, error) {
	asset, err := LoadAsset(addr)
	if err != nil {
		return nil, err
	}

	return asset.History, nil
}
