package context

import (
	"github.com/gorilla/websocket"
	scribble "github.com/nanobox-io/golang-scribble"
	"github.com/nlopes/slack"
	"gitlab.com/Alvoras/telescope/internal/defs"
)

var (
	Db                    *scribble.Driver
	SubChan               chan string
	UnsubChan             chan string
	RecvChan              chan defs.TxMsg
	CloseAPIWebsocketChan chan bool
	EndDiscordSessionChan chan bool
	EndSlackSessionChan   chan bool
	SlackSessionClosed    chan bool
	APIWebsocketClosed    chan bool
	Conn                  *websocket.Conn
	SlackBotClient        *slack.Client
	SlackAppClient        *slack.Client
	Bot                   *slack.UserDetails
	CachedAssets          []defs.Asset
)

func init() {
	SubChan = make(chan string)
	UnsubChan = make(chan string)
	RecvChan = make(chan defs.TxMsg)
	CloseAPIWebsocketChan = make(chan bool, 1)
	EndDiscordSessionChan = make(chan bool, 1)
	EndSlackSessionChan = make(chan bool, 1)
	SlackSessionClosed = make(chan bool, 1)
	APIWebsocketClosed = make(chan bool, 1)
}
