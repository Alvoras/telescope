package wsbchain

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/Alvoras/telescope/internal/config"
	"gitlab.com/Alvoras/telescope/internal/context"
	"gitlab.com/Alvoras/telescope/internal/defs"
	"gitlab.com/Alvoras/telescope/internal/logger"
	"gitlab.com/Alvoras/telescope/internal/utils"
)

func NewConn() (*websocket.Conn, error) {
	var conn *websocket.Conn
	var err error

	for {
		websocket.DefaultDialer.HandshakeTimeout, err = time.ParseDuration(config.Cfg.Timeout.WSHandshake)
		if err != nil {
			logger.Warning.Println("Failed to read websocket handshake timeout : using default")
		}

		conn, _, err = websocket.DefaultDialer.Dial(config.Cfg.URL, nil)
		if err != nil {
			logger.Error.Println(err)
			logger.Info.Println("Retrying handshake...")
			continue
		}

		break
	}

	return conn, nil
}

func StartControlChannel() {
	// Start Control pipeline

	ticker := time.NewTicker(10 * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			// Keep alive connection
			Ping(context.Conn)
			logger.Info.Println("Sent a keepalive message")

		case <-context.CloseAPIWebsocketChan:
			logger.Info.Println("Received CloseAPIWebsocketChan message")
			CloseConn()

			context.APIWebsocketClosed <- true
			logger.Success.Println("API Websocket session terminated")
		}

	}
}

func SubscribeToAllAssets() {
	for _, asset := range context.CachedAssets {
		if asset.Address == "" {
			// Skip first result which seems to always be an empty result...
			continue
		}

		logger.Info.Println(fmt.Sprintf("Subscribing to the %s feed", asset.Address))
		SubscribeToAddr(asset.Address)
	}
}

func ListenForEvents(url string) error {
	var addr string
	var alreadySent []string

	for {
		txMsg := defs.TxMsg{}

		// Block until a message arrive
		_, msg, err := context.Conn.ReadMessage()
		if err != nil {
			return err
		}

		if err := json.Unmarshal(msg, &txMsg.SubTx); err != nil {
			// Print error and wait for another message
			logger.Error.Printf("Failed to parse received data. %s\n", err.Error())
			continue
		}

		// Loop through all addresses of the transaction, check if the address is
		// registered and send to the messaging pipe if it does
	toNextAddress:
		for _, candidate := range txMsg.SubTx.Tx.Addresses() {
			exists, err := utils.AssetExists(candidate)
			if err != nil {
				// Print error and check the next address
				logger.Error.Println(err)
				continue
			}

			if exists {
				for _, alreadySentAddr := range alreadySent {
					if alreadySentAddr == candidate {
						// If already sent, then check the next address in the transaction
						// directly
						continue toNextAddress
					}
				}
			} else {
				continue
			}

			addr = candidate
			alreadySent = append(alreadySent, candidate)

			txMsg.Address = addr

			if txMsg.SubTx != nil {
				context.RecvChan <- txMsg
			}
		}
	}
}

func CloseConn() {
	err := context.Conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil && err != websocket.ErrCloseSent {
		// If close message could not be sent, then close without the handshake.
		context.Conn.Close()
	}
}

func SubscribeToAddr(addr string) {
	cmd := []byte(`{"op":"addr_sub", "addr":"` + addr + `"}`)
	err := context.Conn.WriteMessage(websocket.TextMessage, cmd)
	if err != nil {
		logger.Error.Println("Error subscribing to", addr, err)
	}
}

func UnsubscribeToAddr(addr string) {
	cmd := []byte(`{"op":"addr_unsub", "addr":"` + addr + `"}`)
	err := context.Conn.WriteMessage(websocket.TextMessage, cmd)
	if err != nil {
		logger.Error.Println("Error unsubscribing to", addr, err)
	}
}

func BlockSub(conn *websocket.Conn) {
	cmd := []byte(`{"op":"blocks_sub"}`)
	err := conn.WriteMessage(websocket.TextMessage, cmd)
	if err != nil {
		logger.Error.Println(err)
	}
}

func PingTx(conn *websocket.Conn) {
	cmd := []byte(`{"op":"ping_tx"}`)
	err := conn.WriteMessage(websocket.TextMessage, cmd)
	if err != nil {
		logger.Error.Println(err)
	}
}

func Ping(conn *websocket.Conn) {
	cmd := []byte(`{"op":"ping"}`)
	err := conn.WriteMessage(websocket.TextMessage, cmd)
	if err != nil {
		logger.Error.Println(err)
	}
}
