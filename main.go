package main

import (
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	scribble "github.com/nanobox-io/golang-scribble"
	slacklib "github.com/nlopes/slack"
	"gitlab.com/Alvoras/telescope/internal/config"
	"gitlab.com/Alvoras/telescope/internal/context"
	"gitlab.com/Alvoras/telescope/internal/logger"
	"gitlab.com/Alvoras/telescope/internal/slack"
	"gitlab.com/Alvoras/telescope/internal/utils"
	"gitlab.com/Alvoras/telescope/internal/wsbchain"
)

var (
	db *scribble.Driver
)

func init() {
	logger.Info.Print("Loading config...")
	path, err := filepath.Abs("config.json")
	if err != nil {
		panic(err)
	}

	config.Cfg.Filepath = path
	config.Cfg.Load()

	logger.Success.Println("OK")

	logger.Info.Print("Loading database...")
	context.Db, err = scribble.New("./db", nil)
	if err != nil {
		panic(err)
	}
	logger.Success.Println("OK")

	logger.Info.Println("Caching all the registered assets...")
	_, err = utils.FetchAllAssets()
	if err != nil {
		logger.Error.Panicln("Failed to load the registered addresses")
	}
	logger.Success.Println("OK")

	logger.Info.Print("Loading slack app client...")
	context.SlackAppClient = slacklib.New(config.Cfg.AppToken, slacklib.OptionDebug(false))
	logger.Success.Println("OK")

	logger.Info.Print("Loading slack bot client...")
	context.SlackBotClient = slacklib.New(config.Cfg.BotToken, slacklib.OptionDebug(false))
	logger.Success.Println("OK")

	logger.Info.Println("Synchronizing the assets' channels IDs...")
	if err := slack.SyncAssetsChannels(); err != nil {
		logger.Error.Println("Failed to sync all assets channels :", err)
	} else {
		logger.Success.Println("OK")
	}
}

func main() {
	var err error

	go slack.Start()

	logger.Info.Print("Connecting to the API websocket...")
	context.Conn, err = wsbchain.NewConn()
	if err != nil {
		logger.Error.Println("Failed to connect to the websocket")
		return
	}

	logger.Success.Println("OK")

	logger.Info.Print("Starting control channel...")
	// Used to send closure message when exiting the program, subscribe and unsubscribe to feeds...
	go wsbchain.StartControlChannel()
	logger.Success.Println("OK")

	go func() {
		for {
			logger.Info.Println("Listening for new transactions...")
			err = wsbchain.ListenForEvents(config.Cfg.URL)
			if err != nil {
				if err.Error() == "websocket: close 1000 (normal)" {
					return
				}
				logger.Error.Println(err)
				logger.Error.Println("Lost connection to the websocket, reconnecting...")
				context.Conn, err = wsbchain.NewConn()
				if err != nil {
					logger.Error.Println("Failed to connect to the websocket")
					return
				}

				wsbchain.SubscribeToAllAssets()

				logger.Success.Println("Reconnected")
			}
		}
	}()

	wsbchain.SubscribeToAllAssets()

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)

end:
	for {
		select {
		case txMsg := <-context.RecvChan:
			logger.Info.Println(fmt.Sprintf("New transaction from %s", txMsg.Address))
			slack.AlertNewTx(txMsg)

		case <-sc:
			context.CloseAPIWebsocketChan <- true
			context.EndSlackSessionChan <- true
			logger.Info.Println("Waiting for goroutine to end")
			<-context.SlackSessionClosed
			<-context.APIWebsocketClosed
			break end
		}
	}
}
